ZeroTier 

curl -s 'https://raw.githubusercontent.com/zerotier/ZeroTierOne/master/doc/contact%40zerotier.com.gpg' | gpg --import && \
if z=$(curl -s 'https://install.zerotier.com/' | gpg); then echo "$z" | sudo bash; fi
sudo zerotier-cli join <YOUR_NETWORK_HERE>

SSH tunnel

ssh -R 8080:localhost:80 public.example.com

useSSHTunnel?

snapshot https://snapshots-tezos.giganode.io/snapshot-mainnet_28-06-2021-0132_1534221_BMJuLBZngdG43ufxw96QzXG27Um8peBidhv6ccQrRyNGHWo8qYp.full

https://gitlab.com/tezos/tezos/-/releases

node:
bins
tezos-node
tezos-client # for import only
tezos-baker-*
tezos-endorser-*
tezos-accuser-*

generate identity
./tezos-node identity generate

import snapshot
./tezos-node snapshot import /path/to/chain.full --block <ENTER_BLOCK_HASH_HERE>


signer:
tezos-client
tezos-node # to verify only
tezos-signer 

